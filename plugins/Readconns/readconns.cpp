#include <QDebug>
#include <QString>
#include <QNetworkSession>
#include <QNetworkConfigurationManager>

#include "readconns.h"

using namespace std;

Readconns::Readconns() {

}

QList<QString> Readconns::deviceList() {
  QString QSresult;
  QStringList list;
  QStringList listFin;
  string command = "nmcli -f GENERAL.DEVICE device show";
  array<char, 128> buffer;
  string result;
  FILE* pipe = popen(command.c_str(), "r");
  if (!pipe) {
      qDebug() << "Couldn't start command - nmcli -f GENERAL.DEVICE device show -.";
      return listFin << "--";
  }
  while (fgets(buffer.data(), 128, pipe) != NULL) {
    result += buffer.data();
    QSresult = QString::fromStdString(result);
    QSresult = QSresult.trimmed();
    list = QSresult.split(QRegExp("\\s+"));
  }
  for (int i=0; i < list.count(); ++i) {
    if (!list.value(i).contains(":", Qt::CaseInsensitive)) {
      if (!list.value(i).contains("lo", Qt::CaseInsensitive)) {
        listFin << list.value(i);
      }
    }
  }
  return listFin;
}

QList<QString> Readconns::simSelector(QList<QString> deviceList) {
  QString QSresult;
  QList<QString> QSresultFin;
  string command;
  string commandB = "nmcli -f GENERAL.TYPE device show ";
  array<char, 128> buffer;
  string result;
  for (int i=0; i < deviceList.count(); ++i) {
      command = commandB + deviceList.value(i).toStdString();
      FILE* pipe = popen(command.c_str(), "r");
      if (!pipe) {
          qDebug() << "Couldn't start command - nmcli -f GENERAL.TYPE device show " << deviceList.value(i) << ".";
          return QSresultFin << "--";
      }
      while (fgets(buffer.data(), 128, pipe) != NULL) {
        result += buffer.data();
        QSresult = QString::fromStdString(result);
        int posMid = QSresult.indexOf(":");
        QSresult = QSresult.remove(0, posMid+1);
        QSresult = QSresult.trimmed();
      }
      if (QSresult=="gsm" || QSresult=="cdma" || QSresult=="wimax") {
        QSresultFin << deviceList.value(i);
      }
  }
  if (QSresultFin.count()==0) {
    QSresultFin << "--";
  }
  return QSresultFin;
}

QList<QString> Readconns::SIMconns() {
  QVariant deviceName;
  QString deviceNameStr;
  QList<QString> connsSIM;
  QNetworkInterface interfacesAll;
  QList<QNetworkInterface> interfacesList = interfacesAll.allInterfaces();
  bool isUp=false;
  if (interfacesList.count()>0) {
    int i=1;
    while (i <= interfacesList.count()) {
          deviceName = interfacesList.value(i).name();
          deviceNameStr = deviceName.toString();
          isUp = (interfacesList.value(i).flags() & QNetworkInterface::IsUp);
          if (isUp & deviceNameStr != "wlan0") {
              connsSIM << deviceNameStr;
          }
          i = i+1;
    }
  }
  if (connsSIM.count()==0) {
    connsSIM << "--";
  }
  return connsSIM;
}

QList<QString> Readconns::nameCarrier() {
  bool carrierFound = false;
  QList<QString> nameCarrier;
  QNetworkConfigurationManager manager;
  manager.updateConfigurations();
  QList<QNetworkConfiguration> activeConfigs = manager.allConfigurations(QNetworkConfiguration::Active);
  qDebug() << activeConfigs.count();
  if (activeConfigs.count()>0) {
    for (int i=0; i < activeConfigs.count(); ++i) {
        if (activeConfigs.value(i).bearerTypeFamily()!=2) {
          if (activeConfigs.value(i).name() == "Unknown" || activeConfigs.value(i).bearerTypeName() == "") {
            nameCarrier << "--";
          } else {
            nameCarrier << activeConfigs.value(i).name();
          }
          if (activeConfigs.value(i).bearerTypeName() == "Unknown" || activeConfigs.value(i).bearerTypeName() == "") {
            nameCarrier << "--";
          } else {
            nameCarrier << activeConfigs.value(i).bearerTypeName();
          }
//          nameCarrier << activeConfigs.value(i).identifier();
          carrierFound = true;
        }
    }
  }
  if (carrierFound == false) {
    nameCarrier << "--" << "--";
  }
  return nameCarrier;
}

QList<QString> Readconns::connWifiDetails(QString nameConn) {
  QString QSresult;
  QStringList listEmpty;
  QStringList list;
  QStringList listFin;
  listEmpty << "--" << "--" << "--" << "--" << "--" << "--" << "--" << "--" << "--" << "--";
  if (nameConn == "--") {
    return listEmpty;
  }
  string command = "nmcli dev wifi | grep";
  command = command + " " + nameConn.toStdString();
  array<char, 128> buffer;
  string result;
  FILE* pipe = popen(command.c_str(), "r");
  if (!pipe) {
      qDebug() << "Couldn't start command - nmcli dev wifi | grep -.";
      return listEmpty;
  }
  while (fgets(buffer.data(), 128, pipe) != NULL) {
    result += buffer.data();
    QSresult = QString::fromStdString(result);
    list = QSresult.split(QRegExp("\\s+"));
  }
  for (int i=0; i < list.count(); ++i) {
    if (list.value(i) == "" || list.value(i) == " " || list.value(i) == "undefined") {
         listFin << "--";
    } else {
         listFin << list.value(i);
    }
  }
  return listFin;
}

QList<QString> Readconns::wifiProps(QList<QString> deviceList) {
  QString QSresult;
  QStringList listEmpty;
  QStringList list;
  QStringList listFin;
  listEmpty << "--" << "--" << "--" << "--";
  string commandB = "nmcli -f GENERAL.TYPE,GENERAL.CONNECTION,GENERAL.DRIVER-VERSION dev show ";
  string command;
  array<char, 128> buffer;
  string result;
  for (int i=0; i < deviceList.count(); ++i) {
      command = commandB + deviceList.value(i).toStdString();
      FILE* pipe = popen(command.c_str(), "r");
      if (!pipe) {
          qDebug() << "Couldn't start command // nmcli -f GENERAL.TYPE,GENERAL.CONNECTION,GENERAL.DRIVER-VERSION dev show" << deviceList.value(i) << " //.";
          return listEmpty;
      }
      while (fgets(buffer.data(), 128, pipe) != NULL) {
        result += buffer.data();
        QSresult = QString::fromStdString(result);
        QSresult = QSresult.trimmed();
        list = QSresult.split(QRegExp("\\s+"));
      }
      if (QSresult.contains("wifi", Qt::CaseInsensitive)) {
        list << deviceList.value(i);
        break;
      } else {
        list.clear();
        result = "";
      }
   }
   for (int i=0; i < list.count(); ++i) {
     if (!list.value(i).contains(":", Qt::CaseInsensitive)) {
        if (list.value(i)=="" || list.value(i)==" ") {
          listFin << "--";
        } else {
          listFin << list.value(i);
        }
     }
   }
   return listFin;
}
