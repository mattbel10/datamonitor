#ifndef READCONNS_H
#define READCONNS_H

#include <QObject>
#include <QProcess>

class Readconns: public QObject {
    Q_OBJECT

public:
    Readconns();
    ~Readconns() = default;

    Q_INVOKABLE QList<QString> deviceList();
    Q_INVOKABLE QList<QString> simSelector(QList<QString> deviceList);
    Q_INVOKABLE QList<QString> SIMconns();
    Q_INVOKABLE QList<QString> nameCarrier();
    Q_INVOKABLE QList<QString> connWifiDetails(const QString nameConn);
    Q_INVOKABLE QList<QString> wifiProps(QList<QString> deviceList);

};


#endif
