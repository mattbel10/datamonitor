  function getDatabase() {
       return LocalStorage.openDatabaseSync("themes_db", "1.0", "StorageDatabase", 1000000);
  }

  function createTables() {

      var db = getDatabase();

      db.transaction(
          function(tx) {
              tx.executeSql('CREATE TABLE IF NOT EXISTS storedThemes(threshold_id INTEGER PRIMARY KEY AUTOINCREMENT, theme_name TEXT, backgrd_col TEXT, font_col TEXT, line_col TEXT, butt_col TEXT, grid_col TEXT, bar_col TEXT, opac1 REAL, opac2 REAL, opac3 REAL, opac4 REAL, opac5 REAL, opac6 REAL, index1 INTEGER, index2 INTEGER, index3 INTEGER, index4 INTEGER, index5 INTEGER, index6 INTEGER, daily INTEGER, nightly INTEGER)');
      });
  }

  function insertThemeData(theme_name, theme_colors, theme_opacities, theme_index, daily, nightly){

    var db = getDatabase();

    var res = [];

    db.transaction(function(tx) {

        var rs = tx.executeSql('INSERT INTO storedThemes(theme_name, backgrd_col, font_col, line_col, butt_col, grid_col, bar_col, opac1, opac2, opac3, opac4, opac5, opac6, index1, index2, index3, index4, index5, index6, daily, nightly) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);', [theme_name, theme_colors[0], theme_colors[1], theme_colors[2], theme_colors[3], theme_colors[4], theme_colors[5], theme_opacities[0], theme_opacities[1], theme_opacities[2], theme_opacities[3], theme_opacities[4], theme_opacities[5], theme_index[0], theme_index[1], theme_index[2], theme_index[3], theme_index[4], theme_index[5], daily, nightly]);
        if (rs.rowsAffected > 0) {
            res[0] = "OK";
        } else {
            res[0] = "Error";
        }

        res[1] = getSize() - 1;
    }
    );
    return res;
  }

  function getSize(){

        var db = getDatabase();

        var rs = "";

        db.transaction(function(tx) {
              rs = tx.executeSql("SELECT threshold_id FROM storedThemes t where t.threshold_id");
            }
        );

        if (rs.rows.length > 0) {
          return rs.rows.length;
        } else {
            return 0;
        }
   }

   function getValueFromPos(position){

         var db = getDatabase();

         var rs = "";

         db.transaction(function(tx) {
             rs = tx.executeSql("SELECT theme_name FROM storedThemes t where t.threshold_id");
             }
         );

         if (rs.rows.length > 0) {
             var row = position;
             return rs.rows.item(row).theme_name;
         } else {
             return 0;
         }
    }

   function getThemeFromName(theme_name){

        var themeData = []

        var db = getDatabase();

        var rs = "";

        db.transaction(function(tx) {
            rs = tx.executeSql("SELECT backgrd_col FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[0] = rs.rows.item(0).backgrd_col
            rs = tx.executeSql("SELECT font_col FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[1] = rs.rows.item(0).font_col
            rs = tx.executeSql("SELECT line_col FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[2] = rs.rows.item(0).line_col
            rs = tx.executeSql("SELECT butt_col FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[3] = rs.rows.item(0).butt_col
            rs = tx.executeSql("SELECT grid_col FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[4] = rs.rows.item(0).grid_col
            rs = tx.executeSql("SELECT bar_col FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[5] = rs.rows.item(0).bar_col
            rs = tx.executeSql("SELECT opac1 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[6] = rs.rows.item(0).opac1
            rs = tx.executeSql("SELECT opac2 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[7] = rs.rows.item(0).opac2
            rs = tx.executeSql("SELECT opac3 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[8] = rs.rows.item(0).opac3
            rs = tx.executeSql("SELECT opac4 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[9] = rs.rows.item(0).opac4
            rs = tx.executeSql("SELECT opac5 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[10] = rs.rows.item(0).opac5
            rs = tx.executeSql("SELECT opac6 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[11] = rs.rows.item(0).opac6
            rs = tx.executeSql("SELECT index1 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[12] = rs.rows.item(0).index1
            rs = tx.executeSql("SELECT index2 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[13] = rs.rows.item(0).index2
            rs = tx.executeSql("SELECT index3 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[14] = rs.rows.item(0).index3
            rs = tx.executeSql("SELECT index4 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[15] = rs.rows.item(0).index4
            rs = tx.executeSql("SELECT index5 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[16] = rs.rows.item(0).index5
            rs = tx.executeSql("SELECT index6 FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData[17] = rs.rows.item(0).index6
            }
        );

        return themeData
    }

    function updateTheme(nameTheme, theme_colors, theme_opacities, theme_index){

         var db = getDatabase();

         var rs = "";

         db.transaction(function(tx) {
             rs = tx.executeSql('UPDATE storedThemes SET backgrd_col = ? WHERE theme_name = ?', [theme_colors[0], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET font_col = ? WHERE theme_name = ?', [theme_colors[1], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET line_col = ? WHERE theme_name = ?', [theme_colors[2], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET butt_col = ? WHERE theme_name = ?', [theme_colors[3], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET grid_col = ? WHERE theme_name = ?', [theme_colors[4], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET bar_col = ? WHERE theme_name = ?', [theme_colors[5], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET opac1 = ? WHERE theme_name = ?', [theme_opacities[0], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET opac2 = ? WHERE theme_name = ?', [theme_opacities[1], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET opac3 = ? WHERE theme_name = ?', [theme_opacities[2], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET opac4 = ? WHERE theme_name = ?', [theme_opacities[3], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET opac5 = ? WHERE theme_name = ?', [theme_opacities[4], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET opac6 = ? WHERE theme_name = ?', [theme_opacities[5], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET index1 = ? WHERE theme_name = ?', [theme_index[0], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET index2 = ? WHERE theme_name = ?', [theme_index[1], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET index3 = ? WHERE theme_name = ?', [theme_index[2], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET index4 = ? WHERE theme_name = ?', [theme_index[3], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET index5 = ? WHERE theme_name = ?', [theme_index[4], nameTheme]);
             rs = tx.executeSql('UPDATE storedThemes SET index6 = ? WHERE theme_name = ?', [theme_index[5], nameTheme]);
             }
         );

     }

    function doesExistThemesStorage(){
      createTables();
      var db = getDatabase();
      var rs = "";

      db.transaction(function(tx) {
         rs = tx.executeSql("SELECT t.threshold_id FROM storedThemes t where t.threshold_id");
          }
      );

      if (rs.rows.length == 0) {
        return 0
      } else {
        return 1;
      }
    }

    function doesExistTheme(themeName){

      var db = getDatabase();
      var rs = "";

      db.transaction(function(tx) {
             rs = tx.executeSql("SELECT theme_name FROM storedThemes t where t.theme_name = ?;", [themeName]);
          }
      );

      if (rs.rows.length == 0) {
        return 0
      } else {
        return 1;
      }
    }

    function deleteTheme(name){

         var db = getDatabase();
         var res = "";

         var rs = "";

         db.transaction(function(tx) {
          rs = tx.executeSql("DELETE FROM storedThemes WHERE theme_name = ?", [name]);
             }
         );

         if (rs.rowsAffected > 0) {
             res = "OK";
         } else {
             res = "Error";
         }

         return res;

    }

    function getDailyProp(theme_name){

         var db = getDatabase();

         var themeData = "";

         var rs = "";

         db.transaction(function(tx) {
             rs = tx.executeSql("SELECT daily FROM storedThemes t where t.theme_name = ?;", [theme_name]);
             themeData = rs.rows.item(0).daily
             }
         );
         return themeData
  }

  function getNightlyProp(theme_name){

       var db = getDatabase();

       var themeData = "";

       var rs = "";

       db.transaction(function(tx) {
            rs = tx.executeSql("SELECT nightly FROM storedThemes t where t.theme_name = ?;", [theme_name]);
            themeData = rs.rows.item(0).nightly
           }
       );
       return themeData
   }

   function updateDaily(valueNew, nameTheme){

      var db = getDatabase();

      var rs = "";

      db.transaction(function(tx) {
           rs = tx.executeSql('UPDATE storedThemes SET daily = ? WHERE theme_name = ?', [valueNew, nameTheme]);

         }
      );

    }

    function updateDailyPos(valueNew, position){

       var db = getDatabase();

       var rs = "";

       db.transaction(function(tx) {
           rs = tx.executeSql("SELECT theme_name FROM storedThemes t where t.threshold_id");
           }
       );

       var themeName = rs.rows.item(position).theme_name;

       db.transaction(function(tx) {
            rs = tx.executeSql('UPDATE storedThemes SET daily = ? WHERE theme_name = ?', [valueNew, themeName]);

          }
       );

     }

     function updateNightlyPos(valueNew, position){

        var db = getDatabase();

        var rs = "";

        db.transaction(function(tx) {
            rs = tx.executeSql("SELECT theme_name FROM storedThemes t where t.threshold_id");
            }
        );

        var themeName = rs.rows.item(position).theme_name;

        db.transaction(function(tx) {
             rs = tx.executeSql('UPDATE storedThemes SET nightly = ? WHERE theme_name = ?', [valueNew, themeName]);

           }
        );

      }

      function updateNightly(valueNew, nameTheme){

         var db = getDatabase();

         var rs = "";

         db.transaction(function(tx) {
              rs = tx.executeSql('UPDATE storedThemes SET nightly = ? WHERE theme_name = ?', [valueNew, nameTheme]);

            }
         );

       }
