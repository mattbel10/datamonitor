#!/bin/bash

set -e

rm /home/phablet/.config/upstart/mattdaemon.conf
rm /home/phablet/.config/upstart/mattdaemon-service.conf

echo "mattdaemon.conf and mattdaemon-service.conf uninstalled"
