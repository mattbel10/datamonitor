# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the datamonitor.matteobellei package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: datamonitor.matteobellei\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-10-18 17:34+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../indicator/mbellei-indicator-datamonitor.py:77
msgid "NOT AVAILABLE"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:180
msgid "Hide indicator icon"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:188
msgid "Status: ACTIVE"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:193
msgid "Status: INACTIVE"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:198
msgid "Status: UNKNOWN"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:201
msgid "Updated:"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:204
msgid "dataMonitor's data usage information:"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:209
msgid "SIM DATA USAGE:"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:214
#: ../indicator/mbellei-indicator-datamonitor.py:232
msgid "Today data: "
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:219
#: ../indicator/mbellei-indicator-datamonitor.py:237
msgid "Total month data: "
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:227
msgid "WI-FI DATA USAGE:"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:243
msgid "dataMonitor App and Settings"
msgstr ""

#: ../indicator/mbellei-indicator-datamonitor.py:278 ../qml/AboutPage.qml:53
#: datamonitor.desktop.in.h:1
msgid "dataMonitor"
msgstr ""

#: ../qml/AboutPage.qml:9 ../qml/Main.qml:213
msgid "About"
msgstr ""

#: ../qml/AboutPage.qml:73
msgid "Version: "
msgstr ""

#: ../qml/AboutPage.qml:82
msgid ""
"This application permits to monitor the data usage for the two main device "
"data connections (SIM and Wi-Fi)."
msgstr ""

#: ../qml/AboutPage.qml:92
msgid ""
"This program is free software: you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation, either version 3 of the License, or (at your option) "
"any later version. This program is distributed in the hope that it will be "
"useful, but WITHOUT ANY WARRANTY; without even the implied warranty of "
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the <a "
"href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU General Public "
"License</a> for more details."
msgstr ""

#: ../qml/AboutPage.qml:102
msgid "ISSUES AND BUG REPORTING"
msgstr ""

#: ../qml/AboutPage.qml:102
msgid "SOURCE CODE"
msgstr ""

#: ../qml/AboutPage.qml:113
msgid "Copyright"
msgstr ""

#: ../qml/AboutPage.qml:122
msgid "Gestures and hidden functions"
msgstr ""

#: ../qml/AboutPage.qml:137
msgid "Credits"
msgstr ""

#: ../qml/ChartPage.qml:124
msgid "Days of Month"
msgstr ""

#: ../qml/ChartPage.qml:126
msgid "Wi-fi analytic page"
msgstr ""

#: ../qml/ChartPage.qml:130
msgid "SIM analytic page"
msgstr ""

#: ../qml/ChartPage.qml:260
msgid "Graph gestures complete list available at app 'Information' page"
msgstr ""

#: ../qml/ChartPage.qml:265
msgid "Date:"
msgstr ""

#: ../qml/ChartPage.qml:266
msgid "Total data usage:"
msgstr ""

#: ../qml/ChartPage.qml:267
msgid "Daily data usage:"
msgstr ""

#: ../qml/ChartPage.qml:330
msgid "Data threshold for -"
msgstr ""

#: ../qml/ChartPage.qml:330
msgid "set at"
msgstr ""

#: ../qml/ChartPage.qml:330
msgid "was exceeded!"
msgstr ""

#: ../qml/ChartPage.qml:339
msgid "Data not available"
msgstr ""

#: ../qml/ChartPage.qml:388 ../qml/ChartPage.qml:401
msgid "Data thresholds list"
msgstr ""

#: ../qml/ChartPage.qml:413
msgid "No data thresholds available"
msgstr ""

#: ../qml/ChartPage.qml:512
msgid "Edit data threshold"
msgstr ""

#: ../qml/ChartPage.qml:513
msgid "Only an integer type number is allowed in the text field."
msgstr ""

#: ../qml/ChartPage.qml:526 ../qml/PrefPage.qml:414
msgid "Save"
msgstr ""

#: ../qml/ChartPage.qml:581
msgid ""
"ERROR: zero figure (0) is not allowed. Correct it or press the 'Cancel' "
"button to go back to the thresholds list."
msgstr ""

#: ../qml/ChartPage.qml:583
msgid ""
"ERROR: number is already in the list. Modify it or press the 'Cancel' button "
"to go back to the thresholds list."
msgstr ""

#: ../qml/ChartPage.qml:588
msgid ""
"ERROR: typed figure is not a number. Correct it or press the 'Cancel' button "
"to go back to the thresholds list."
msgstr ""

#: ../qml/ChartPage.qml:593 ../qml/PrefPage.qml:56
msgid "Cancel"
msgstr ""

#: ../qml/CreditsPage.qml:9
msgid "About - Credits"
msgstr ""

#: ../qml/CreditsPage.qml:45
msgid ""
"Thanks to anyone who contributed or just supported the present app. I wish "
"to mention those people who I believe have contributed the most:"
msgstr ""

#: ../qml/CreditsPage.qml:54
msgid ""
"he is the author of the current graphical look of the app. He mostly worked "
"on the .qml side of the code. Thank you Michele."
msgstr ""

#: ../qml/CreditsPage.qml:63
msgid ""
"he is one of the most important contributors to 'clickable' and he also "
"tinkered the .qml code of the present app to permit it to compile for the "
"arm64 architecture in an agnostic and automatic way. Thank you Jonnie."
msgstr ""

#: ../qml/CreditsPage.qml:72
msgid ""
"a big thanks to @Pgcor from the UBports italian channel, because the "
"development of the present app was brought further even when I got no device "
"to test modifications. He successfully tested several app versions on behalf "
"of me, without tiredness."
msgstr ""

#: ../qml/CreditsPage.qml:81
msgid ""
"thanks to him, dataMonitor has got a new fancy icon. He has a very good "
"artistic touch and helped me a lot to improving my original idea of a "
"suitable icon starting from the gist of my app. Thank you Constantin."
msgstr ""

#: ../qml/CreditsPage.qml:90
msgid ""
"he is the creator of 'clickable', the most simple and powerful tool to be "
"soon productive to developing for the Ubuntu Touch platform. A big thanks "
"goes to Brian as well."
msgstr ""

#: ../qml/GesturesPage.qml:9
msgid "About - Gestures and hidden functions"
msgstr ""

#: ../qml/GesturesPage.qml:45
msgid ""
"There are some gestures and hidden functions in the app useful to interact "
"with the graph. Let's introduce them."
msgstr ""

#: ../qml/GesturesPage.qml:54
msgid "Gestures (only graph):"
msgstr ""

#: ../qml/GesturesPage.qml:63
msgid ""
"- Drag downward/upward with two fingers pressed on graph: user can "
"respectively enlarge/narrow the data range."
msgstr ""

#: ../qml/GesturesPage.qml:72
msgid ""
"- Drag rightward/leftward with one finger pressed on graph: user can "
"respectively scroll backward/forward into the received data of the next/"
"earlier month."
msgstr ""

#: ../qml/GesturesPage.qml:81
msgid ""
"- Keep one finger pressed on graph: a data threshold is generated on the "
"graph. A list of thresholds is immediately visible soon after generation by "
"dragging up the 'BottomEdge' handle."
msgstr ""

#: ../qml/GesturesPage.qml:90
msgid ""
"- Touch with one finger a graph bar (fast click): a message bubble is "
"displayed with data recap for the concerned day."
msgstr ""

#: ../qml/GesturesPage.qml:99
msgid "Hidden functions (only graph):"
msgstr ""

#: ../qml/GesturesPage.qml:108
msgid ""
"- Threshold list shown in the 'BottomEdge' - THRESHOLD DELETION: for every "
"row corresponding to a single generated threshold, user can delete a "
"threshold by pressing on the selected row and dragging towards right to show "
"the deletion option and then he can press the relevant icon to perform the "
"chosen action."
msgstr ""

#: ../qml/GesturesPage.qml:117
msgid ""
"- Threshold list shown in the 'BottomEdge' - THRESHOLD MUTING/UNMUTING: for "
"every row corresponding to a single generated threshold, user can mute a "
"threshold by pressing on the selected row and dragging towards left to show "
"the muting/unmuting option and then pressing the relevant icon to perform "
"the chosen action. The muting is kept indefinitely in time and it is "
"reversible following the same procedure."
msgstr ""

#: ../qml/GesturesPage.qml:126
msgid ""
"- Threshold list shown in the 'BottomEdge' - THRESHOLD EDIT: for every row "
"corresponding to a single generated threshold, user can edit the relevant "
"data threshold by pressing on the selected row and dragging towards left to "
"show the edit option and then pressing the relevant icon to perform the "
"chosen action. A dialog with a text field is opened to edit the selected "
"figure."
msgstr ""

#: ../qml/Main.qml:159
msgid ""
"Daemon currently uninstalled. Just close and re-open the app to restore it "
"back."
msgstr ""

#: ../qml/Main.qml:204
msgid "Data usage monitor"
msgstr ""

#: ../qml/Main.qml:208 ../qml/PrefPage.qml:89
msgid "Settings"
msgstr ""

#: ../qml/Main.qml:282 ../qml/Main.qml:380
msgid "Today received data"
msgstr ""

#: ../qml/Main.qml:286 ../qml/Main.qml:312 ../qml/Main.qml:383
#: ../qml/Main.qml:409
msgid "in MBytes"
msgstr ""

#: ../qml/Main.qml:308 ../qml/Main.qml:405
msgid "Month total received data"
msgstr ""

#: ../qml/Main.qml:335
msgid "SIM data chart"
msgstr ""

#: ../qml/Main.qml:350 ../qml/SimDetailsPage.qml:16
msgid "SIM details"
msgstr ""

#: ../qml/Main.qml:431
msgid "WI-FI data chart"
msgstr ""

#: ../qml/Main.qml:447 ../qml/WifiDetailsPage.qml:14
msgid "WI-FI details"
msgstr ""

#: ../qml/Main.qml:476
msgid ""
"Daemon automatically installed. Reboot the phone to set the data usage "
"monitoring application online."
msgstr ""

#: ../qml/Main.qml:480
msgid "Failed to install daemon."
msgstr ""

#: ../qml/PrefPage.qml:31
msgid ""
"Color palettes are locked. To unlock, tap on the lock icon on the top right "
"corner of the present sub-section."
msgstr ""

#: ../qml/PrefPage.qml:32
msgid ""
"Slider is locked. To unlock it, tap on the lock icon on the top right corner "
"of the present sub-section."
msgstr ""

#: ../qml/PrefPage.qml:35
msgid "-New theme-"
msgstr ""

#: ../qml/PrefPage.qml:36
msgid "Manage theme"
msgstr ""

#: ../qml/PrefPage.qml:37
msgid "Save theme"
msgstr ""

#: ../qml/PrefPage.qml:40
msgid "Default theme removal"
msgstr ""

#: ../qml/PrefPage.qml:41
msgid ""
"dataMonitor default theme cannot be removed. Press 'Cancel' to go back to "
"'Settings'."
msgstr ""

#: ../qml/PrefPage.qml:42
msgid "Theme option deselection not permitted"
msgstr ""

#: ../qml/PrefPage.qml:43
msgid ""
"In order to facilitate the task and to avoid having no theme with the this "
"option enabled, it can be disabled for the present theme only by enabling "
"the same option for an other theme in its place."
msgstr ""

#: ../qml/PrefPage.qml:46
msgid "Operation accomplished"
msgstr ""

#: ../qml/PrefPage.qml:47
msgid "Theme removed with success. Press 'Return' to go back to 'Settings'."
msgstr ""

#: ../qml/PrefPage.qml:48
msgid "Theme saved with success. Press 'Return' to go back to 'Settings'."
msgstr ""

#: ../qml/PrefPage.qml:49
msgid "Operation not accomplished"
msgstr ""

#: ../qml/PrefPage.qml:50
msgid ""
"Theme wasn't removed due to an internal error. Press 'Return' to go back to "
"'Settings'."
msgstr ""

#: ../qml/PrefPage.qml:51
msgid ""
"Theme wasn't saved due to an internal error. Press 'Return' to go back to "
"'Settings'."
msgstr ""

#: ../qml/PrefPage.qml:52
msgid ""
"Enable global Theme to follow the day light (SuruDark or Ambiance themes) by "
"setting the time for the switch"
msgstr ""

#: ../qml/PrefPage.qml:53
msgid ""
"Enable custom Theme to follow the day light by setting the time for the "
"switch"
msgstr ""

#: ../qml/PrefPage.qml:55
msgid "Return"
msgstr ""

#: ../qml/PrefPage.qml:59
msgid "to"
msgstr ""

#: ../qml/PrefPage.qml:60
msgid "Daily switch (from 'SuruDark' to 'Ambiance')"
msgstr ""

#: ../qml/PrefPage.qml:61
msgid "Daily custom switch (from"
msgstr ""

#: ../qml/PrefPage.qml:64
msgid "Nightly switch (from 'Ambiance' to 'SuruDark')"
msgstr ""

#: ../qml/PrefPage.qml:65
msgid "Nightly custom switch (from"
msgstr ""

#: ../qml/PrefPage.qml:158
msgid "Enable global Theme style from OS general settings"
msgstr ""

#: ../qml/PrefPage.qml:325
msgid "Colors Palettes:"
msgstr ""

#: ../qml/PrefPage.qml:354
msgid "Custom theme:"
msgstr ""

#: ../qml/PrefPage.qml:406
msgid "Save a new theme"
msgstr ""

#: ../qml/PrefPage.qml:407
msgid ""
"Properties modified in the color palettes area are stored as a new theme "
"with name."
msgstr ""

#: ../qml/PrefPage.qml:410
msgid "-Write new theme name here-"
msgstr ""

#: ../qml/PrefPage.qml:422
msgid "Existing theme"
msgstr ""

#: ../qml/PrefPage.qml:465
msgid ""
"Remove an existing custom theme or update its properties (daily/nightly)"
msgstr ""

#: ../qml/PrefPage.qml:468
msgid "Theme set as daily"
msgstr ""

#: ../qml/PrefPage.qml:500
msgid "Theme set as nightly"
msgstr ""

#: ../qml/PrefPage.qml:531
msgid "Remove theme"
msgstr ""

#: ../qml/PrefPage.qml:645
msgid "Do you wish to overwrite existing theme?"
msgstr ""

#: ../qml/PrefPage.qml:647
msgid "Yes"
msgstr ""

#: ../qml/PrefPage.qml:672
msgid "Background color:"
msgstr ""

#: ../qml/PrefPage.qml:676
msgid "Font color:"
msgstr ""

#: ../qml/PrefPage.qml:680
msgid "Lines color:"
msgstr ""

#: ../qml/PrefPage.qml:684
msgid "Buttons color:"
msgstr ""

#: ../qml/PrefPage.qml:688
msgid "Grid color (Graph only):"
msgstr ""

#: ../qml/PrefPage.qml:692
msgid "Bars fill color (Graph only):"
msgstr ""

#: ../qml/PrefPage.qml:769
msgid "Opacity:"
msgstr ""

#: ../qml/PrefPage.qml:834
msgid "Daemon Settings:"
msgstr ""

#: ../qml/PrefPage.qml:859
msgid "Set time step for daemon in idle activity"
msgstr ""

#: ../qml/PrefPage.qml:871
msgid ""
"The smaller it is, the more it affects battery drain but a big time step may "
"seldom lead to incoherent data storage"
msgstr ""

#. TRANSLATORS: min and sec refer to the abbreviated words for minutes and seconds
#: ../qml/PrefPage.qml:912
msgid "%1 min and %2 sec"
msgstr ""

#: ../qml/PrefPage.qml:914
msgid "%1 sec"
msgstr ""

#: ../qml/PrefPage.qml:960
msgid ""
"Remove daemon to save battery. This action will put offline the monitoring "
"of received data"
msgstr ""

#: ../qml/PrefPage.qml:967
msgid "Remove daemon"
msgstr ""

#: ../qml/PrefPage.qml:990
msgid "Indicator Settings:"
msgstr ""

#: ../qml/PrefPage.qml:1002
msgid ""
"Install dataMonitor indicator to have the data usage monitoring always at "
"your finger tap"
msgstr ""

#: ../qml/PrefPage.qml:1009
msgid "Install Indicator"
msgstr ""

#: ../qml/PrefPage.qml:1009
msgid "Uninstall Indicator"
msgstr ""

#: ../qml/PrefPage.qml:1048
msgid "Display today SIM data usage in the indicator"
msgstr ""

#: ../qml/PrefPage.qml:1073
msgid "Display total SIM data usage in the indicator"
msgstr ""

#: ../qml/PrefPage.qml:1098
msgid "Display today Wi-Fi data usage in the indicator"
msgstr ""

#: ../qml/PrefPage.qml:1123
msgid "Display total Wi-Fi data usage in the indicator"
msgstr ""

#: ../qml/PrefPage.qml:1158
msgid ""
"Daemon files removed. DataMonitor is offline. Close and open again the "
"'dataMonitor' app to get daemon files automatically installed."
msgstr ""

#: ../qml/PrefPage.qml:1162
msgid "Failed to uninstall both the daemon files."
msgstr ""

#: ../qml/PrefPage.qml:1170
msgid ""
"Indicator successfully installed. For the first installation, the icon might "
"not be immediately visible within the indicator bar, therefore a reboot is "
"necessary."
msgstr ""

#: ../qml/PrefPage.qml:1174
msgid "Failed to install indicator."
msgstr ""

#: ../qml/PrefPage.qml:1182
msgid "Indicator successfully uninstalled."
msgstr ""

#: ../qml/PrefPage.qml:1186
msgid "Failed to uninstall indicator."
msgstr ""

#: ../qml/SimDetailsPage.qml:74 ../qml/WifiDetailsPage.qml:65
msgid "General device name:"
msgstr ""

#: ../qml/SimDetailsPage.qml:93
msgid "Device name:"
msgstr ""

#: ../qml/SimDetailsPage.qml:112
msgid "Carrier name:"
msgstr ""

#: ../qml/SimDetailsPage.qml:131
msgid "Bearer channel:"
msgstr ""

#: ../qml/WifiDetailsPage.qml:84
msgid "Driver version:"
msgstr ""

#: ../qml/WifiDetailsPage.qml:102
msgid "Connection name:"
msgstr ""

#: ../qml/WifiDetailsPage.qml:121
msgid "Connection type:"
msgstr ""

#: ../qml/WifiDetailsPage.qml:140
msgid "Connection security:"
msgstr ""

#: ../qml/WifiDetailsPage.qml:158
msgid "Connection velocity:"
msgstr ""

#: ../qml/WifiDetailsPage.qml:176
msgid "Connection channel:"
msgstr ""

#: ../qml/WifiDetailsPage.qml:194
msgid "Connection strength:"
msgstr ""
